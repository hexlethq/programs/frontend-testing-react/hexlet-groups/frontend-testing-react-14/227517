const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('testing sort function', () => {
  test('must not to be the same array', () => {
    fc.assert(
      fc.property(fc.tuple(fc.integer(), fc.string()), (data) => {
        const sorted = sort(data);
        expect(sorted).not.toBe(data);
      }),
    );
  });

  test('should contain the same items', () => {
    const count = (tab, element) => tab.filter((v) => v === element).length;
    fc.assert(
      fc.property(fc.array(fc.integer()), (data) => {
        const sorted = sort(data);
        expect(sorted.length).toEqual(data.length);
        data.forEach((item) => {
          expect(count(sorted, item)).toEqual(count(data, item));
        });
      }),
    );
  });

  test('must be sorted (string values)', () => {
    fc.assert(
      fc.property(fc.array(fc.string()), (data) => {
        const sorted = sort(data);
        expect(sorted).toBeSorted({ coerce: true });
      }),
    );
  });

  test('must be sorted (float values)', () => {
    fc.assert(
      fc.property(fc.array(fc.float()), (data) => {
        const sorted = sort(data);
        expect(sorted).toBeSorted();
      }),
    );
  });
});
// END
