const faker = require('faker');

// BEGIN
describe('a testing of function helpers.createTransaction() from faker.js', () => {
  test('received data is Object', () => {
    const data = faker.helpers.createTransaction();
    expect(data).toBeInstanceOf(Object);
  });

  describe('function provide unique data, except date', () => {
    const controlArr = Array(3).fill(faker.helpers.createTransaction).map((f) => f());

    test.each(controlArr)('recived data, %s', (item) => {
      const data = faker.helpers.createTransaction();
      expect(item).not.toBe(data);
      expect(item).not.toMatchObject(faker.helpers.createTransaction());
    });
  });

  test('a testing property of received object', () => {
    const data = faker.helpers.createTransaction();
    expect(data).toEqual({
      amount: expect.stringMatching(/[0-9]+.[0-9]{2}/),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.stringMatching(/[0-9]+/),
    });
  });
});
// END
