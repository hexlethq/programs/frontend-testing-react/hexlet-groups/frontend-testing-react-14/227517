const assert = require('power-assert');
const { flattenDepth } = require('lodash');

// BEGIN
const data = [1, [2, 3], [[4], 5]];
assert.deepStrictEqual(flattenDepth([]), []);
assert.deepStrictEqual(flattenDepth([], 5), []);
assert.deepStrictEqual(flattenDepth(data), [1, 2, 3, [4], 5]);
assert.deepStrictEqual(flattenDepth(data, 2), [1, 2, 3, 4, 5]);
assert.deepStrictEqual(flattenDepth(data, 5), [1, 2, 3, 4, 5]);
assert.deepStrictEqual(flattenDepth(data, '2'), [1, 2, 3, 4, 5]);
assert.deepStrictEqual(flattenDepth(data, -2), data);
assert.deepStrictEqual(flattenDepth(data, 0), data);
assert.deepStrictEqual(flattenDepth(data, null), data);
assert.deepStrictEqual(flattenDepth(data, 'word'), data);
assert.deepStrictEqual(flattenDepth(data, false), data);
assert.deepStrictEqual(flattenDepth(data, true), [1, 2, 3, [4], 5]);
assert.deepStrictEqual(flattenDepth(data, { key: 'value' }), data);
assert.deepStrictEqual(flattenDepth(data, (x) => x), data);
assert.deepStrictEqual(flattenDepth({ key: 'value' }), []);
assert.deepStrictEqual(flattenDepth('word'), ['w', 'o', 'r', 'd']);
assert.deepStrictEqual(flattenDepth(5), []);
// END

// ПЕРЕД ТЕМ КАК НАЧАТЬ РЕШАТЬ ЗАДАНИЕ
// УДАЛИТЕ СЛЕДУЮЩУЮ СТРОКУ
// assert.ok(done);
