const fs = require('fs');

// BEGIN
const path = require('path');

const getAbsPath = (filepath) => path.resolve(__dirname, filepath);
const getNewPackage = (data, param) => {
  try {
    JSON.parse(data);
  } catch (err) {
    throw new Error('Package parsing error');
  }
  const pack = JSON.parse(data);
  const vertionParts = pack.version.split('.');
  const mapping = {
    major: (arr) => [Number(arr[0]) + 1, 0, 0],
    minor: (arr) => [arr[0], Number(arr[1]) + 1, 0],
    patch: (arr) => [arr[0], arr[1], Number(arr[2]) + 1],
  };
  if (!mapping[param]) {
    throw new Error(`${param} - is faulty param naming`);
  }
  const newVersion = mapping[param](vertionParts).join('.');
  return JSON.stringify({ ...pack, version: newVersion });
};

const upVersion = (filepath, param = 'patch') => {
  const content = fs.readFileSync(getAbsPath(filepath), 'utf-8');
  const proccessedData = getNewPackage(content, param);
  fs.writeFileSync(getAbsPath(filepath), proccessedData, 'utf-8');
};

// END

module.exports = { upVersion };
