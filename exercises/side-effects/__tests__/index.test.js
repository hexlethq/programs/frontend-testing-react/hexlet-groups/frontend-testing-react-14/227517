const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

// BEGIN
const getFixturePath = (filename) => path.join(__dirname, '..', '__fixtures__', filename);

describe('testing function upVersion', () => {
  const dataForTesting = [
    ['major', '2.0.0'],
    ['minor', '1.4.0'],
    ['patch', '1.3.3'],
    [undefined, '1.3.3'],
  ];

  test.each(dataForTesting)('with param (%s)', (param, expData) => {
    const filepathForTestingFunction = getFixturePath('package.json');
    const fixtureFilename = path.resolve(__dirname, filepathForTestingFunction);

    upVersion(filepathForTestingFunction, param);

    const content = fs.readFileSync(fixtureFilename, 'utf-8');
    const { version } = JSON.parse(content);

    expect(version).toBe(expData);
  });

  test('Wrong param name', () => {
    const filepathForTestingFunction = getFixturePath('package.json');
    expect(() => upVersion(filepathForTestingFunction, 'faultyParam'))
      .toThrowError(new Error('faultyParam - is faulty param naming'));
  });

  test('Wrong data', () => {
    const faultyDataFilepath = getFixturePath('wrongPackage.json');
    expect(() => upVersion(faultyDataFilepath))
      .toThrowError(new Error('Package parsing error'));
  });

  test('Wrong file name', () => {
    expect(() => upVersion('./wrong/filename')).toThrowErrorMatchingSnapshot();
  });

  afterEach(() => {
    const fixtureFilename = path.resolve(__dirname, getFixturePath('package.json'));
    fs.writeFileSync(fixtureFilename, '{"version":"1.3.2"}', 'utf-8');
  });
});
// END
