const axios = require('axios');

// BEGIN
const get = async (filepath) => {
  try {
    const response = await axios.get(filepath);
    return response.data;
  } catch (e) {
    const { response } = e;
    if (response && response.status >= 500) {
      throw new Error('Error on the server');
    }
    if (response && response.status >= 400) {
      throw new Error('Wrong request');
    }
    throw e;
  }
};

const post = async (filepath, data) => {
  try {
    const response = await axios.post(filepath, data);
    return response.data;
  } catch (e) {
    const { response } = e;
    if (response && response.status >= 500) {
      throw new Error('Error on the server');
    }
    if (response && response.status >= 400) {
      throw new Error('Wrong request or data');
    }
    throw e;
  }
};
// END

module.exports = { get, post };
