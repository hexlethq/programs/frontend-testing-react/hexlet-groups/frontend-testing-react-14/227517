const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN
describe('testing http metods', () => {
  const url = new URL('https://example.com/users');
  const content = { firstname: 'Fedor', lastname: 'Sumkin', age: 33 };
  const postResponse = { id: 10 };

  test('successful application of function get', async () => {
    const scope = nock(url.origin).get(url.pathname).reply(200, [content]);
    const response = await get(url.href);
    expect(scope.isDone()).toBeTruthy();
    expect(response).toEqual([content]);
  });

  test('successful application of function post', async () => {
    const scope = nock(url.origin).post(url.pathname, content).reply(200, postResponse);
    const response = await post(url.href, content);
    expect(scope.isDone()).toBeTruthy();
    expect(response).toEqual(postResponse);
  });

  test('unsuccessful application of function get', async () => {
    const scope1 = nock(url.origin).get('/wrong-path').reply(404);
    const scope2 = nock(url.origin).get(url.pathname).reply(500);
    await expect(get('https://example.com/wrong-path')).rejects
      .toMatchObject({ message: 'Wrong request' });
    await expect(get(url.href)).rejects.toMatchObject({ message: 'Error on the server' });
    expect(scope1.isDone()).toBeTruthy();
    expect(scope2.isDone()).toBeTruthy();
  });

  test('net error', async () => {
    nock.disableNetConnect();
    await expect(get(url.href)).rejects.toThrow();
    await expect(post(url.href, content)).rejects.toThrow();
  });

  test('unsuccessful application of function post', async () => {
    const scope1 = nock(url.origin).post('/wrong-path', content).reply(404);
    const scope2 = nock(url.origin).post(url.pathname, content).reply(500);
    await expect(post('https://example.com/wrong-path', content)).rejects
      .toMatchObject({ message: 'Wrong request or data' });
    await expect(post(url.href, content)).rejects.toMatchObject({ message: 'Error on the server' });
    expect(scope1.isDone()).toBeTruthy();
    expect(scope2.isDone()).toBeTruthy();
  });
});
// END
